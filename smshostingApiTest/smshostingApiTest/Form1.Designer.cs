﻿namespace smshostingApiTest
{
    partial class Form1
    {
       /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbAuth_key = new System.Windows.Forms.TextBox();
            this.tbAuth_secret = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSmsSearchAccept = new System.Windows.Forms.TabControl();
            this.tabUser = new System.Windows.Forms.TabPage();
            this.label34 = new System.Windows.Forms.Label();
            this.tbUserAccept = new System.Windows.Forms.ComboBox();
            this.tbUserResult = new System.Windows.Forms.TextBox();
            this.bGetUser = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbUserUrl = new System.Windows.Forms.TextBox();
            this.tabSmsSend = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.tbSmsSendAccept = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbSmsSendEncoding = new System.Windows.Forms.ComboBox();
            this.tbSmsSendSandbox = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tbSmsSendCallback = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tbSmsSendTransaction = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tbSmsSendDate = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbSmsSendGroup = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbSmsSendFrom = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbSmsSendText = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbSmsSendTo = new System.Windows.Forms.TextBox();
            this.bSmsSendPost = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.tbSmsSendUrl = new System.Windows.Forms.TextBox();
            this.tbSmsSendResult = new System.Windows.Forms.TextBox();
            this.tabSmsSendbulk = new System.Windows.Forms.TabPage();
            this.label49 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkAccept = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkEncoding = new System.Windows.Forms.ComboBox();
            this.tbSmsSendbulkSandbox = new System.Windows.Forms.CheckBox();
            this.label52 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkCallback = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkTransaction = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkDate = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkGroup = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkFrom = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkText = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkTo = new System.Windows.Forms.TextBox();
            this.bSmsSendbulkPost = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.tbSmsSendbulkUrl = new System.Windows.Forms.TextBox();
            this.tbSmsSendbulkResult = new System.Windows.Forms.TextBox();
            this.tabSmsEstimate = new System.Windows.Forms.TabPage();
            this.label60 = new System.Windows.Forms.Label();
            this.tbSmsEstimateAccept = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.tbSmsEstimateEncoding = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tbSmsEstimateGroup = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.tbSmsEstimateFrom = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.tbSmsEstimateText = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.tbSmsEstimateTo = new System.Windows.Forms.TextBox();
            this.bSmsEstimatePost = new System.Windows.Forms.Button();
            this.label70 = new System.Windows.Forms.Label();
            this.tbSmsEstimateUrl = new System.Windows.Forms.TextBox();
            this.tbSmsEstimateResult = new System.Windows.Forms.TextBox();
            this.tabSmsCancel = new System.Windows.Forms.TabPage();
            this.label61 = new System.Windows.Forms.Label();
            this.tbSmsCancelAccept = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this.tbSmsCancelId = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.tbSmsCancelTransaction = new System.Windows.Forms.TextBox();
            this.bSmsCancelPost = new System.Windows.Forms.Button();
            this.label73 = new System.Windows.Forms.Label();
            this.tbSmsCancelUrl = new System.Windows.Forms.TextBox();
            this.tbSmsCancelResult = new System.Windows.Forms.TextBox();
            this.tabSmsSearch = new System.Windows.Forms.TabPage();
            this.label48 = new System.Windows.Forms.Label();
            this.tbSmsSearchLimit = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tbSmsSearchOffset = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.tbSmsSearchStatus = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tbSmsSearchToDate = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tbSmsSearchFromDate = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbSmsSearchMsisdn = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbSmsSearchTransactionId = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cbSmsSearchAccept = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbSmsSearchId = new System.Windows.Forms.TextBox();
            this.tbSmsSearchResult = new System.Windows.Forms.TextBox();
            this.bSmsSearchGET = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.tbSmsSearchUrl = new System.Windows.Forms.TextBox();
            this.tsbSmsRecvSearch = new System.Windows.Forms.TabPage();
            this.label78 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchLimit = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchOffset = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchToDate = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchAccept = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchFromDate = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchSimRefId = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchFrom = new System.Windows.Forms.TextBox();
            this.tbSmsRecvSearchResult = new System.Windows.Forms.TextBox();
            this.bSmsRecvSearchGet = new System.Windows.Forms.Button();
            this.label75 = new System.Windows.Forms.Label();
            this.tbSmsRecvSearchUrl = new System.Windows.Forms.TextBox();
            this.tabSmsSimList = new System.Windows.Forms.TabPage();
            this.label79 = new System.Windows.Forms.Label();
            this.tbSimListAccept = new System.Windows.Forms.ComboBox();
            this.tbSimListResult = new System.Windows.Forms.TextBox();
            this.bSimListGet = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.tbSimListUrl = new System.Windows.Forms.TextBox();
            this.tabOtpSend = new System.Windows.Forms.TabPage();
            this.label31 = new System.Windows.Forms.Label();
            this.tbOtpSendAccept = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbOtpSendEncoding = new System.Windows.Forms.ComboBox();
            this.cbOtpSendSandbox = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbOtpSendTtl = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbOtpSendMaxRetry = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbOtpSendCodLen = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbOtpSendAppId = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbOtpSendFrom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbOtpSendText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbOtpSendTo = new System.Windows.Forms.TextBox();
            this.tbSendResult = new System.Windows.Forms.TextBox();
            this.bOtpSendPost = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbOtpSendUrl = new System.Windows.Forms.TextBox();
            this.tabOtpCheck = new System.Windows.Forms.TabPage();
            this.label30 = new System.Windows.Forms.Label();
            this.tbOtpCheckAccept = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbOtpCheckIp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbOtpCheckCode = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbOtpCheckVerifyId = new System.Windows.Forms.TextBox();
            this.tbCheckResult = new System.Windows.Forms.TextBox();
            this.bOtpCheckGet = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.tbOtpCheckUrl = new System.Windows.Forms.TextBox();
            this.tabOtpSearch = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.cbOtpSearchAccept = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbOtpSearchVerifyId = new System.Windows.Forms.TextBox();
            this.tbSearchResult = new System.Windows.Forms.TextBox();
            this.bOtpSearchGet = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.tbOtpSearchUrl = new System.Windows.Forms.TextBox();
            this.tabOtpCommand = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.tbOtpCommandAccept = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbOtpCommandCommand = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbOtpCommandVerifyId = new System.Windows.Forms.TextBox();
            this.tbCommandResult = new System.Windows.Forms.TextBox();
            this.bOtpCommandPost = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.tbOtpCommandUrl = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tbSmsSearchAccept.SuspendLayout();
            this.tabUser.SuspendLayout();
            this.tabSmsSend.SuspendLayout();
            this.tabSmsSendbulk.SuspendLayout();
            this.tabSmsEstimate.SuspendLayout();
            this.tabSmsCancel.SuspendLayout();
            this.tabSmsSearch.SuspendLayout();
            this.tsbSmsRecvSearch.SuspendLayout();
            this.tabSmsSimList.SuspendLayout();
            this.tabOtpSend.SuspendLayout();
            this.tabOtpCheck.SuspendLayout();
            this.tabOtpSearch.SuspendLayout();
            this.tabOtpCommand.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "AUTH_KEY ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tbAuth_key
            // 
            this.tbAuth_key.Location = new System.Drawing.Point(111, 12);
            this.tbAuth_key.Name = "tbAuth_key";
            this.tbAuth_key.Size = new System.Drawing.Size(204, 20);
            this.tbAuth_key.TabIndex = 1;
            // 
            // tbAuth_secret
            // 
            this.tbAuth_secret.Location = new System.Drawing.Point(448, 12);
            this.tbAuth_secret.Name = "tbAuth_secret";
            this.tbAuth_secret.Size = new System.Drawing.Size(252, 20);
            this.tbAuth_secret.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(353, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "AUTH_SECRET ";
            // 
            // tbSmsSearchAccept
            // 
            this.tbSmsSearchAccept.Controls.Add(this.tabUser);
            this.tbSmsSearchAccept.Controls.Add(this.tabSmsSend);
            this.tbSmsSearchAccept.Controls.Add(this.tabSmsSendbulk);
            this.tbSmsSearchAccept.Controls.Add(this.tabSmsEstimate);
            this.tbSmsSearchAccept.Controls.Add(this.tabSmsCancel);
            this.tbSmsSearchAccept.Controls.Add(this.tabSmsSearch);
            this.tbSmsSearchAccept.Controls.Add(this.tsbSmsRecvSearch);
            this.tbSmsSearchAccept.Controls.Add(this.tabSmsSimList);
            this.tbSmsSearchAccept.Controls.Add(this.tabOtpSend);
            this.tbSmsSearchAccept.Controls.Add(this.tabOtpCheck);
            this.tbSmsSearchAccept.Controls.Add(this.tabOtpSearch);
            this.tbSmsSearchAccept.Controls.Add(this.tabOtpCommand);
            this.tbSmsSearchAccept.Location = new System.Drawing.Point(30, 67);
            this.tbSmsSearchAccept.Multiline = true;
            this.tbSmsSearchAccept.Name = "tbSmsSearchAccept";
            this.tbSmsSearchAccept.SelectedIndex = 0;
            this.tbSmsSearchAccept.Size = new System.Drawing.Size(930, 548);
            this.tbSmsSearchAccept.TabIndex = 4;
            // 
            // tabUser
            // 
            this.tabUser.Controls.Add(this.label34);
            this.tabUser.Controls.Add(this.tbUserAccept);
            this.tabUser.Controls.Add(this.tbUserResult);
            this.tabUser.Controls.Add(this.bGetUser);
            this.tabUser.Controls.Add(this.label3);
            this.tabUser.Controls.Add(this.tbUserUrl);
            this.tabUser.Location = new System.Drawing.Point(4, 22);
            this.tabUser.Name = "tabUser";
            this.tabUser.Padding = new System.Windows.Forms.Padding(3);
            this.tabUser.Size = new System.Drawing.Size(922, 522);
            this.tabUser.TabIndex = 0;
            this.tabUser.Text = "User";
            this.tabUser.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(757, 56);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 13);
            this.label34.TabIndex = 19;
            this.label34.Text = "Accept";
            // 
            // tbUserAccept
            // 
            this.tbUserAccept.FormattingEnabled = true;
            this.tbUserAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbUserAccept.Location = new System.Drawing.Point(758, 76);
            this.tbUserAccept.Name = "tbUserAccept";
            this.tbUserAccept.Size = new System.Drawing.Size(153, 21);
            this.tbUserAccept.TabIndex = 18;
            this.tbUserAccept.Text = "application/json";
            // 
            // tbUserResult
            // 
            this.tbUserResult.Location = new System.Drawing.Point(18, 103);
            this.tbUserResult.Multiline = true;
            this.tbUserResult.Name = "tbUserResult";
            this.tbUserResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbUserResult.Size = new System.Drawing.Size(893, 409);
            this.tbUserResult.TabIndex = 3;
            // 
            // bGetUser
            // 
            this.bGetUser.Location = new System.Drawing.Point(758, 16);
            this.bGetUser.Name = "bGetUser";
            this.bGetUser.Size = new System.Drawing.Size(153, 29);
            this.bGetUser.TabIndex = 2;
            this.bGetUser.Text = "GET";
            this.bGetUser.UseVisualStyleBackColor = true;
            this.bGetUser.Click += new System.EventHandler(this.bGetUser_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Url";
            // 
            // tbUserUrl
            // 
            this.tbUserUrl.Location = new System.Drawing.Point(70, 21);
            this.tbUserUrl.Name = "tbUserUrl";
            this.tbUserUrl.Size = new System.Drawing.Size(665, 20);
            this.tbUserUrl.TabIndex = 0;
            this.tbUserUrl.Text = "https://api.smshosting.it/rest/api/user";
            // 
            // tabSmsSend
            // 
            this.tabSmsSend.Controls.Add(this.label7);
            this.tabSmsSend.Controls.Add(this.tbSmsSendAccept);
            this.tabSmsSend.Controls.Add(this.label8);
            this.tabSmsSend.Controls.Add(this.label9);
            this.tabSmsSend.Controls.Add(this.tbSmsSendEncoding);
            this.tabSmsSend.Controls.Add(this.tbSmsSendSandbox);
            this.tabSmsSend.Controls.Add(this.label33);
            this.tabSmsSend.Controls.Add(this.tbSmsSendCallback);
            this.tabSmsSend.Controls.Add(this.label35);
            this.tabSmsSend.Controls.Add(this.tbSmsSendTransaction);
            this.tabSmsSend.Controls.Add(this.label36);
            this.tabSmsSend.Controls.Add(this.tbSmsSendDate);
            this.tabSmsSend.Controls.Add(this.label37);
            this.tabSmsSend.Controls.Add(this.tbSmsSendGroup);
            this.tabSmsSend.Controls.Add(this.label38);
            this.tabSmsSend.Controls.Add(this.tbSmsSendFrom);
            this.tabSmsSend.Controls.Add(this.label39);
            this.tabSmsSend.Controls.Add(this.tbSmsSendText);
            this.tabSmsSend.Controls.Add(this.label40);
            this.tabSmsSend.Controls.Add(this.tbSmsSendTo);
            this.tabSmsSend.Controls.Add(this.bSmsSendPost);
            this.tabSmsSend.Controls.Add(this.label41);
            this.tabSmsSend.Controls.Add(this.tbSmsSendUrl);
            this.tabSmsSend.Controls.Add(this.tbSmsSendResult);
            this.tabSmsSend.Location = new System.Drawing.Point(4, 22);
            this.tabSmsSend.Name = "tabSmsSend";
            this.tabSmsSend.Size = new System.Drawing.Size(922, 522);
            this.tabSmsSend.TabIndex = 2;
            this.tabSmsSend.Text = "SMS send";
            this.tabSmsSend.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(752, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 50;
            this.label7.Text = "Accept";
            // 
            // tbSmsSendAccept
            // 
            this.tbSmsSendAccept.FormattingEnabled = true;
            this.tbSmsSendAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbSmsSendAccept.Location = new System.Drawing.Point(753, 72);
            this.tbSmsSendAccept.Name = "tbSmsSendAccept";
            this.tbSmsSendAccept.Size = new System.Drawing.Size(153, 21);
            this.tbSmsSendAccept.TabIndex = 49;
            this.tbSmsSendAccept.Text = "application/json";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "sandbox";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 257);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "encoding";
            // 
            // tbSmsSendEncoding
            // 
            this.tbSmsSendEncoding.FormattingEnabled = true;
            this.tbSmsSendEncoding.Items.AddRange(new object[] {
            "7BIT",
            "UCS2",
            "AUTO"});
            this.tbSmsSendEncoding.Location = new System.Drawing.Point(107, 254);
            this.tbSmsSendEncoding.Name = "tbSmsSendEncoding";
            this.tbSmsSendEncoding.Size = new System.Drawing.Size(103, 21);
            this.tbSmsSendEncoding.TabIndex = 46;
            this.tbSmsSendEncoding.Text = "7BIT";
            // 
            // tbSmsSendSandbox
            // 
            this.tbSmsSendSandbox.AutoSize = true;
            this.tbSmsSendSandbox.Checked = true;
            this.tbSmsSendSandbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tbSmsSendSandbox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbSmsSendSandbox.Location = new System.Drawing.Point(107, 205);
            this.tbSmsSendSandbox.Name = "tbSmsSendSandbox";
            this.tbSmsSendSandbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbSmsSendSandbox.Size = new System.Drawing.Size(15, 14);
            this.tbSmsSendSandbox.TabIndex = 45;
            this.tbSmsSendSandbox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tbSmsSendSandbox.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(27, 231);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 13);
            this.label33.TabIndex = 44;
            this.label33.Text = "status callback";
            // 
            // tbSmsSendCallback
            // 
            this.tbSmsSendCallback.Location = new System.Drawing.Point(107, 228);
            this.tbSmsSendCallback.Name = "tbSmsSendCallback";
            this.tbSmsSendCallback.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendCallback.TabIndex = 43;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(27, 182);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 13);
            this.label35.TabIndex = 42;
            this.label35.Text = "Transaction id";
            // 
            // tbSmsSendTransaction
            // 
            this.tbSmsSendTransaction.Location = new System.Drawing.Point(107, 179);
            this.tbSmsSendTransaction.Name = "tbSmsSendTransaction";
            this.tbSmsSendTransaction.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendTransaction.TabIndex = 41;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(27, 156);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 40;
            this.label36.Text = "date";
            // 
            // tbSmsSendDate
            // 
            this.tbSmsSendDate.Location = new System.Drawing.Point(107, 153);
            this.tbSmsSendDate.Name = "tbSmsSendDate";
            this.tbSmsSendDate.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendDate.TabIndex = 39;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(27, 101);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(34, 13);
            this.label37.TabIndex = 38;
            this.label37.Text = "group";
            // 
            // tbSmsSendGroup
            // 
            this.tbSmsSendGroup.Location = new System.Drawing.Point(107, 98);
            this.tbSmsSendGroup.Name = "tbSmsSendGroup";
            this.tbSmsSendGroup.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendGroup.TabIndex = 37;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(27, 48);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "from";
            // 
            // tbSmsSendFrom
            // 
            this.tbSmsSendFrom.Location = new System.Drawing.Point(107, 45);
            this.tbSmsSendFrom.Name = "tbSmsSendFrom";
            this.tbSmsSendFrom.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendFrom.TabIndex = 35;
            this.tbSmsSendFrom.Text = "SMSHosting";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(27, 130);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(24, 13);
            this.label39.TabIndex = 34;
            this.label39.Text = "text";
            // 
            // tbSmsSendText
            // 
            this.tbSmsSendText.Location = new System.Drawing.Point(107, 127);
            this.tbSmsSendText.Name = "tbSmsSendText";
            this.tbSmsSendText.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendText.TabIndex = 33;
            this.tbSmsSendText.Text = "Ciao";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(27, 75);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(16, 13);
            this.label40.TabIndex = 32;
            this.label40.Text = "to";
            // 
            // tbSmsSendTo
            // 
            this.tbSmsSendTo.Location = new System.Drawing.Point(107, 72);
            this.tbSmsSendTo.Name = "tbSmsSendTo";
            this.tbSmsSendTo.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendTo.TabIndex = 31;
            // 
            // bSmsSendPost
            // 
            this.bSmsSendPost.Location = new System.Drawing.Point(755, 14);
            this.bSmsSendPost.Name = "bSmsSendPost";
            this.bSmsSendPost.Size = new System.Drawing.Size(153, 29);
            this.bSmsSendPost.TabIndex = 30;
            this.bSmsSendPost.Text = "POST";
            this.bSmsSendPost.UseVisualStyleBackColor = true;
            this.bSmsSendPost.Click += new System.EventHandler(this.bSmsSendPost_Click_1);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(27, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 29;
            this.label41.Text = "Url";
            // 
            // tbSmsSendUrl
            // 
            this.tbSmsSendUrl.Location = new System.Drawing.Point(107, 19);
            this.tbSmsSendUrl.Name = "tbSmsSendUrl";
            this.tbSmsSendUrl.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendUrl.TabIndex = 28;
            this.tbSmsSendUrl.Text = "https://api.smshosting.it/rest/api/sms/send";
            // 
            // tbSmsSendResult
            // 
            this.tbSmsSendResult.Location = new System.Drawing.Point(15, 293);
            this.tbSmsSendResult.Multiline = true;
            this.tbSmsSendResult.Name = "tbSmsSendResult";
            this.tbSmsSendResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSmsSendResult.Size = new System.Drawing.Size(893, 212);
            this.tbSmsSendResult.TabIndex = 15;
            // 
            // tabSmsSendbulk
            // 
            this.tabSmsSendbulk.Controls.Add(this.label49);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkAccept);
            this.tabSmsSendbulk.Controls.Add(this.label50);
            this.tabSmsSendbulk.Controls.Add(this.label51);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkEncoding);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkSandbox);
            this.tabSmsSendbulk.Controls.Add(this.label52);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkCallback);
            this.tabSmsSendbulk.Controls.Add(this.label53);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkTransaction);
            this.tabSmsSendbulk.Controls.Add(this.label54);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkDate);
            this.tabSmsSendbulk.Controls.Add(this.label55);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkGroup);
            this.tabSmsSendbulk.Controls.Add(this.label56);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkFrom);
            this.tabSmsSendbulk.Controls.Add(this.label57);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkText);
            this.tabSmsSendbulk.Controls.Add(this.label58);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkTo);
            this.tabSmsSendbulk.Controls.Add(this.bSmsSendbulkPost);
            this.tabSmsSendbulk.Controls.Add(this.label59);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkUrl);
            this.tabSmsSendbulk.Controls.Add(this.tbSmsSendbulkResult);
            this.tabSmsSendbulk.Location = new System.Drawing.Point(4, 22);
            this.tabSmsSendbulk.Name = "tabSmsSendbulk";
            this.tabSmsSendbulk.Size = new System.Drawing.Size(922, 522);
            this.tabSmsSendbulk.TabIndex = 7;
            this.tabSmsSendbulk.Text = "SMS sendbulk";
            this.tabSmsSendbulk.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(752, 54);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 13);
            this.label49.TabIndex = 74;
            this.label49.Text = "Accept";
            // 
            // tbSmsSendbulkAccept
            // 
            this.tbSmsSendbulkAccept.FormattingEnabled = true;
            this.tbSmsSendbulkAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbSmsSendbulkAccept.Location = new System.Drawing.Point(753, 74);
            this.tbSmsSendbulkAccept.Name = "tbSmsSendbulkAccept";
            this.tbSmsSendbulkAccept.Size = new System.Drawing.Size(153, 21);
            this.tbSmsSendbulkAccept.TabIndex = 73;
            this.tbSmsSendbulkAccept.Text = "application/json";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(27, 211);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(47, 13);
            this.label50.TabIndex = 72;
            this.label50.Text = "sandbox";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(27, 259);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(51, 13);
            this.label51.TabIndex = 71;
            this.label51.Text = "encoding";
            // 
            // tbSmsSendbulkEncoding
            // 
            this.tbSmsSendbulkEncoding.FormattingEnabled = true;
            this.tbSmsSendbulkEncoding.Items.AddRange(new object[] {
            "7BIT",
            "UCS2",
            "AUTO"});
            this.tbSmsSendbulkEncoding.Location = new System.Drawing.Point(107, 256);
            this.tbSmsSendbulkEncoding.Name = "tbSmsSendbulkEncoding";
            this.tbSmsSendbulkEncoding.Size = new System.Drawing.Size(103, 21);
            this.tbSmsSendbulkEncoding.TabIndex = 70;
            this.tbSmsSendbulkEncoding.Text = "7BIT";
            // 
            // tbSmsSendbulkSandbox
            // 
            this.tbSmsSendbulkSandbox.AutoSize = true;
            this.tbSmsSendbulkSandbox.Checked = true;
            this.tbSmsSendbulkSandbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tbSmsSendbulkSandbox.Location = new System.Drawing.Point(107, 207);
            this.tbSmsSendbulkSandbox.Name = "tbSmsSendbulkSandbox";
            this.tbSmsSendbulkSandbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbSmsSendbulkSandbox.Size = new System.Drawing.Size(15, 14);
            this.tbSmsSendbulkSandbox.TabIndex = 69;
            this.tbSmsSendbulkSandbox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tbSmsSendbulkSandbox.UseVisualStyleBackColor = true;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(27, 233);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(78, 13);
            this.label52.TabIndex = 68;
            this.label52.Text = "status callback";
            // 
            // tbSmsSendbulkCallback
            // 
            this.tbSmsSendbulkCallback.Location = new System.Drawing.Point(107, 230);
            this.tbSmsSendbulkCallback.Name = "tbSmsSendbulkCallback";
            this.tbSmsSendbulkCallback.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkCallback.TabIndex = 67;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(27, 184);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(74, 13);
            this.label53.TabIndex = 66;
            this.label53.Text = "Transaction id";
            // 
            // tbSmsSendbulkTransaction
            // 
            this.tbSmsSendbulkTransaction.Location = new System.Drawing.Point(107, 181);
            this.tbSmsSendbulkTransaction.Name = "tbSmsSendbulkTransaction";
            this.tbSmsSendbulkTransaction.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkTransaction.TabIndex = 65;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(27, 158);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 64;
            this.label54.Text = "date";
            // 
            // tbSmsSendbulkDate
            // 
            this.tbSmsSendbulkDate.Location = new System.Drawing.Point(107, 155);
            this.tbSmsSendbulkDate.Name = "tbSmsSendbulkDate";
            this.tbSmsSendbulkDate.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkDate.TabIndex = 63;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(27, 103);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 13);
            this.label55.TabIndex = 62;
            this.label55.Text = "group";
            // 
            // tbSmsSendbulkGroup
            // 
            this.tbSmsSendbulkGroup.Location = new System.Drawing.Point(107, 100);
            this.tbSmsSendbulkGroup.Name = "tbSmsSendbulkGroup";
            this.tbSmsSendbulkGroup.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkGroup.TabIndex = 61;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(27, 50);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(27, 13);
            this.label56.TabIndex = 60;
            this.label56.Text = "from";
            // 
            // tbSmsSendbulkFrom
            // 
            this.tbSmsSendbulkFrom.Location = new System.Drawing.Point(107, 47);
            this.tbSmsSendbulkFrom.Name = "tbSmsSendbulkFrom";
            this.tbSmsSendbulkFrom.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkFrom.TabIndex = 59;
            this.tbSmsSendbulkFrom.Text = "SMSHosting";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(27, 132);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(24, 13);
            this.label57.TabIndex = 58;
            this.label57.Text = "text";
            // 
            // tbSmsSendbulkText
            // 
            this.tbSmsSendbulkText.Location = new System.Drawing.Point(107, 129);
            this.tbSmsSendbulkText.Name = "tbSmsSendbulkText";
            this.tbSmsSendbulkText.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkText.TabIndex = 57;
            this.tbSmsSendbulkText.Text = "Ciao";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(27, 77);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(16, 13);
            this.label58.TabIndex = 56;
            this.label58.Text = "to";
            // 
            // tbSmsSendbulkTo
            // 
            this.tbSmsSendbulkTo.Location = new System.Drawing.Point(107, 74);
            this.tbSmsSendbulkTo.Name = "tbSmsSendbulkTo";
            this.tbSmsSendbulkTo.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkTo.TabIndex = 55;
            // 
            // bSmsSendbulkPost
            // 
            this.bSmsSendbulkPost.Location = new System.Drawing.Point(755, 16);
            this.bSmsSendbulkPost.Name = "bSmsSendbulkPost";
            this.bSmsSendbulkPost.Size = new System.Drawing.Size(153, 29);
            this.bSmsSendbulkPost.TabIndex = 54;
            this.bSmsSendbulkPost.Text = "POST";
            this.bSmsSendbulkPost.UseVisualStyleBackColor = true;
            this.bSmsSendbulkPost.Click += new System.EventHandler(this.bSmsSendbulkPost_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(27, 24);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(20, 13);
            this.label59.TabIndex = 53;
            this.label59.Text = "Url";
            // 
            // tbSmsSendbulkUrl
            // 
            this.tbSmsSendbulkUrl.Location = new System.Drawing.Point(107, 21);
            this.tbSmsSendbulkUrl.Name = "tbSmsSendbulkUrl";
            this.tbSmsSendbulkUrl.Size = new System.Drawing.Size(625, 20);
            this.tbSmsSendbulkUrl.TabIndex = 52;
            this.tbSmsSendbulkUrl.Text = "https://api.smshosting.it/rest/api/sms/sendbulk";
            // 
            // tbSmsSendbulkResult
            // 
            this.tbSmsSendbulkResult.Location = new System.Drawing.Point(15, 295);
            this.tbSmsSendbulkResult.Multiline = true;
            this.tbSmsSendbulkResult.Name = "tbSmsSendbulkResult";
            this.tbSmsSendbulkResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSmsSendbulkResult.Size = new System.Drawing.Size(893, 212);
            this.tbSmsSendbulkResult.TabIndex = 51;
            // 
            // tabSmsEstimate
            // 
            this.tabSmsEstimate.Controls.Add(this.label60);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateAccept);
            this.tabSmsEstimate.Controls.Add(this.label62);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateEncoding);
            this.tabSmsEstimate.Controls.Add(this.label66);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateGroup);
            this.tabSmsEstimate.Controls.Add(this.label67);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateFrom);
            this.tabSmsEstimate.Controls.Add(this.label68);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateText);
            this.tabSmsEstimate.Controls.Add(this.label69);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateTo);
            this.tabSmsEstimate.Controls.Add(this.bSmsEstimatePost);
            this.tabSmsEstimate.Controls.Add(this.label70);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateUrl);
            this.tabSmsEstimate.Controls.Add(this.tbSmsEstimateResult);
            this.tabSmsEstimate.Location = new System.Drawing.Point(4, 22);
            this.tabSmsEstimate.Name = "tabSmsEstimate";
            this.tabSmsEstimate.Size = new System.Drawing.Size(922, 522);
            this.tabSmsEstimate.TabIndex = 8;
            this.tabSmsEstimate.Text = "SMS estimate";
            this.tabSmsEstimate.UseVisualStyleBackColor = true;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(752, 54);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(41, 13);
            this.label60.TabIndex = 98;
            this.label60.Text = "Accept";
            // 
            // tbSmsEstimateAccept
            // 
            this.tbSmsEstimateAccept.FormattingEnabled = true;
            this.tbSmsEstimateAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbSmsEstimateAccept.Location = new System.Drawing.Point(753, 74);
            this.tbSmsEstimateAccept.Name = "tbSmsEstimateAccept";
            this.tbSmsEstimateAccept.Size = new System.Drawing.Size(153, 21);
            this.tbSmsEstimateAccept.TabIndex = 97;
            this.tbSmsEstimateAccept.Text = "application/json";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(27, 158);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(51, 13);
            this.label62.TabIndex = 95;
            this.label62.Text = "encoding";
            // 
            // tbSmsEstimateEncoding
            // 
            this.tbSmsEstimateEncoding.FormattingEnabled = true;
            this.tbSmsEstimateEncoding.Items.AddRange(new object[] {
            "7BIT",
            "UCS2",
            "AUTO"});
            this.tbSmsEstimateEncoding.Location = new System.Drawing.Point(107, 155);
            this.tbSmsEstimateEncoding.Name = "tbSmsEstimateEncoding";
            this.tbSmsEstimateEncoding.Size = new System.Drawing.Size(103, 21);
            this.tbSmsEstimateEncoding.TabIndex = 94;
            this.tbSmsEstimateEncoding.Text = "7BIT";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(27, 103);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(34, 13);
            this.label66.TabIndex = 86;
            this.label66.Text = "group";
            // 
            // tbSmsEstimateGroup
            // 
            this.tbSmsEstimateGroup.Location = new System.Drawing.Point(107, 100);
            this.tbSmsEstimateGroup.Name = "tbSmsEstimateGroup";
            this.tbSmsEstimateGroup.Size = new System.Drawing.Size(625, 20);
            this.tbSmsEstimateGroup.TabIndex = 85;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(27, 50);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(27, 13);
            this.label67.TabIndex = 84;
            this.label67.Text = "from";
            // 
            // tbSmsEstimateFrom
            // 
            this.tbSmsEstimateFrom.Location = new System.Drawing.Point(107, 47);
            this.tbSmsEstimateFrom.Name = "tbSmsEstimateFrom";
            this.tbSmsEstimateFrom.Size = new System.Drawing.Size(625, 20);
            this.tbSmsEstimateFrom.TabIndex = 83;
            this.tbSmsEstimateFrom.Text = "SMSHosting";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(27, 132);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(24, 13);
            this.label68.TabIndex = 82;
            this.label68.Text = "text";
            // 
            // tbSmsEstimateText
            // 
            this.tbSmsEstimateText.Location = new System.Drawing.Point(107, 129);
            this.tbSmsEstimateText.Name = "tbSmsEstimateText";
            this.tbSmsEstimateText.Size = new System.Drawing.Size(625, 20);
            this.tbSmsEstimateText.TabIndex = 81;
            this.tbSmsEstimateText.Text = "Ciao";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(27, 77);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(16, 13);
            this.label69.TabIndex = 80;
            this.label69.Text = "to";
            // 
            // tbSmsEstimateTo
            // 
            this.tbSmsEstimateTo.Location = new System.Drawing.Point(107, 74);
            this.tbSmsEstimateTo.Name = "tbSmsEstimateTo";
            this.tbSmsEstimateTo.Size = new System.Drawing.Size(625, 20);
            this.tbSmsEstimateTo.TabIndex = 79;
            // 
            // bSmsEstimatePost
            // 
            this.bSmsEstimatePost.Location = new System.Drawing.Point(755, 16);
            this.bSmsEstimatePost.Name = "bSmsEstimatePost";
            this.bSmsEstimatePost.Size = new System.Drawing.Size(153, 29);
            this.bSmsEstimatePost.TabIndex = 78;
            this.bSmsEstimatePost.Text = "POST";
            this.bSmsEstimatePost.UseVisualStyleBackColor = true;
            this.bSmsEstimatePost.Click += new System.EventHandler(this.bSmsEstimatePost_Click);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(27, 24);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(20, 13);
            this.label70.TabIndex = 77;
            this.label70.Text = "Url";
            // 
            // tbSmsEstimateUrl
            // 
            this.tbSmsEstimateUrl.Location = new System.Drawing.Point(107, 21);
            this.tbSmsEstimateUrl.Name = "tbSmsEstimateUrl";
            this.tbSmsEstimateUrl.Size = new System.Drawing.Size(625, 20);
            this.tbSmsEstimateUrl.TabIndex = 76;
            this.tbSmsEstimateUrl.Text = "https://api.smshosting.it/rest/api/sms/estimate";
            // 
            // tbSmsEstimateResult
            // 
            this.tbSmsEstimateResult.Location = new System.Drawing.Point(15, 194);
            this.tbSmsEstimateResult.Multiline = true;
            this.tbSmsEstimateResult.Name = "tbSmsEstimateResult";
            this.tbSmsEstimateResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSmsEstimateResult.Size = new System.Drawing.Size(893, 313);
            this.tbSmsEstimateResult.TabIndex = 75;
            // 
            // tabSmsCancel
            // 
            this.tabSmsCancel.Controls.Add(this.label61);
            this.tabSmsCancel.Controls.Add(this.tbSmsCancelAccept);
            this.tabSmsCancel.Controls.Add(this.label65);
            this.tabSmsCancel.Controls.Add(this.tbSmsCancelId);
            this.tabSmsCancel.Controls.Add(this.label72);
            this.tabSmsCancel.Controls.Add(this.tbSmsCancelTransaction);
            this.tabSmsCancel.Controls.Add(this.bSmsCancelPost);
            this.tabSmsCancel.Controls.Add(this.label73);
            this.tabSmsCancel.Controls.Add(this.tbSmsCancelUrl);
            this.tabSmsCancel.Controls.Add(this.tbSmsCancelResult);
            this.tabSmsCancel.Location = new System.Drawing.Point(4, 22);
            this.tabSmsCancel.Name = "tabSmsCancel";
            this.tabSmsCancel.Size = new System.Drawing.Size(922, 522);
            this.tabSmsCancel.TabIndex = 9;
            this.tabSmsCancel.Text = "SMS cancel";
            this.tabSmsCancel.UseVisualStyleBackColor = true;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(752, 54);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(41, 13);
            this.label61.TabIndex = 114;
            this.label61.Text = "Accept";
            // 
            // tbSmsCancelAccept
            // 
            this.tbSmsCancelAccept.FormattingEnabled = true;
            this.tbSmsCancelAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbSmsCancelAccept.Location = new System.Drawing.Point(753, 74);
            this.tbSmsCancelAccept.Name = "tbSmsCancelAccept";
            this.tbSmsCancelAccept.Size = new System.Drawing.Size(153, 21);
            this.tbSmsCancelAccept.TabIndex = 113;
            this.tbSmsCancelAccept.Text = "application/json";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(27, 50);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(15, 13);
            this.label65.TabIndex = 108;
            this.label65.Text = "id";
            // 
            // tbSmsCancelId
            // 
            this.tbSmsCancelId.Location = new System.Drawing.Point(107, 47);
            this.tbSmsCancelId.Name = "tbSmsCancelId";
            this.tbSmsCancelId.Size = new System.Drawing.Size(625, 20);
            this.tbSmsCancelId.TabIndex = 107;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(27, 77);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(68, 13);
            this.label72.TabIndex = 104;
            this.label72.Text = "transactionId";
            // 
            // tbSmsCancelTransaction
            // 
            this.tbSmsCancelTransaction.Location = new System.Drawing.Point(107, 74);
            this.tbSmsCancelTransaction.Name = "tbSmsCancelTransaction";
            this.tbSmsCancelTransaction.Size = new System.Drawing.Size(625, 20);
            this.tbSmsCancelTransaction.TabIndex = 103;
            // 
            // bSmsCancelPost
            // 
            this.bSmsCancelPost.Location = new System.Drawing.Point(755, 16);
            this.bSmsCancelPost.Name = "bSmsCancelPost";
            this.bSmsCancelPost.Size = new System.Drawing.Size(153, 29);
            this.bSmsCancelPost.TabIndex = 102;
            this.bSmsCancelPost.Text = "POST";
            this.bSmsCancelPost.UseVisualStyleBackColor = true;
            this.bSmsCancelPost.Click += new System.EventHandler(this.bSmsCancelPost_Click);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(27, 24);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(20, 13);
            this.label73.TabIndex = 101;
            this.label73.Text = "Url";
            // 
            // tbSmsCancelUrl
            // 
            this.tbSmsCancelUrl.Location = new System.Drawing.Point(107, 21);
            this.tbSmsCancelUrl.Name = "tbSmsCancelUrl";
            this.tbSmsCancelUrl.Size = new System.Drawing.Size(625, 20);
            this.tbSmsCancelUrl.TabIndex = 100;
            this.tbSmsCancelUrl.Text = "https://api.smshosting.it/rest/api/sms/cancel";
            // 
            // tbSmsCancelResult
            // 
            this.tbSmsCancelResult.Location = new System.Drawing.Point(15, 112);
            this.tbSmsCancelResult.Multiline = true;
            this.tbSmsCancelResult.Name = "tbSmsCancelResult";
            this.tbSmsCancelResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSmsCancelResult.Size = new System.Drawing.Size(893, 395);
            this.tbSmsCancelResult.TabIndex = 99;
            // 
            // tabSmsSearch
            // 
            this.tabSmsSearch.Controls.Add(this.label48);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchLimit);
            this.tabSmsSearch.Controls.Add(this.label47);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchOffset);
            this.tabSmsSearch.Controls.Add(this.label46);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchStatus);
            this.tabSmsSearch.Controls.Add(this.label45);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchToDate);
            this.tabSmsSearch.Controls.Add(this.label44);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchFromDate);
            this.tabSmsSearch.Controls.Add(this.label43);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchMsisdn);
            this.tabSmsSearch.Controls.Add(this.label42);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchTransactionId);
            this.tabSmsSearch.Controls.Add(this.label32);
            this.tabSmsSearch.Controls.Add(this.cbSmsSearchAccept);
            this.tabSmsSearch.Controls.Add(this.label26);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchId);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchResult);
            this.tabSmsSearch.Controls.Add(this.bSmsSearchGET);
            this.tabSmsSearch.Controls.Add(this.label27);
            this.tabSmsSearch.Controls.Add(this.tbSmsSearchUrl);
            this.tabSmsSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSmsSearch.Name = "tabSmsSearch";
            this.tabSmsSearch.Size = new System.Drawing.Size(922, 522);
            this.tabSmsSearch.TabIndex = 6;
            this.tabSmsSearch.Text = "SMS search";
            this.tabSmsSearch.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(27, 231);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(24, 13);
            this.label48.TabIndex = 37;
            this.label48.Text = "limit";
            // 
            // tbSmsSearchLimit
            // 
            this.tbSmsSearchLimit.Location = new System.Drawing.Point(101, 228);
            this.tbSmsSearchLimit.Name = "tbSmsSearchLimit";
            this.tbSmsSearchLimit.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchLimit.TabIndex = 36;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(27, 205);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(33, 13);
            this.label47.TabIndex = 35;
            this.label47.Text = "offset";
            // 
            // tbSmsSearchOffset
            // 
            this.tbSmsSearchOffset.Location = new System.Drawing.Point(101, 202);
            this.tbSmsSearchOffset.Name = "tbSmsSearchOffset";
            this.tbSmsSearchOffset.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchOffset.TabIndex = 34;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(27, 179);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(35, 13);
            this.label46.TabIndex = 33;
            this.label46.Text = "status";
            // 
            // tbSmsSearchStatus
            // 
            this.tbSmsSearchStatus.Location = new System.Drawing.Point(101, 176);
            this.tbSmsSearchStatus.Name = "tbSmsSearchStatus";
            this.tbSmsSearchStatus.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchStatus.TabIndex = 32;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(27, 153);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(39, 13);
            this.label45.TabIndex = 31;
            this.label45.Text = "toDate";
            // 
            // tbSmsSearchToDate
            // 
            this.tbSmsSearchToDate.Location = new System.Drawing.Point(101, 150);
            this.tbSmsSearchToDate.Name = "tbSmsSearchToDate";
            this.tbSmsSearchToDate.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchToDate.TabIndex = 30;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(27, 127);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(50, 13);
            this.label44.TabIndex = 29;
            this.label44.Text = "fromDate";
            // 
            // tbSmsSearchFromDate
            // 
            this.tbSmsSearchFromDate.Location = new System.Drawing.Point(101, 124);
            this.tbSmsSearchFromDate.Name = "tbSmsSearchFromDate";
            this.tbSmsSearchFromDate.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchFromDate.TabIndex = 28;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(27, 101);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(39, 13);
            this.label43.TabIndex = 27;
            this.label43.Text = "msisdn";
            // 
            // tbSmsSearchMsisdn
            // 
            this.tbSmsSearchMsisdn.Location = new System.Drawing.Point(101, 98);
            this.tbSmsSearchMsisdn.Name = "tbSmsSearchMsisdn";
            this.tbSmsSearchMsisdn.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchMsisdn.TabIndex = 26;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(27, 75);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(68, 13);
            this.label42.TabIndex = 25;
            this.label42.Text = "transactionId";
            // 
            // tbSmsSearchTransactionId
            // 
            this.tbSmsSearchTransactionId.Location = new System.Drawing.Point(101, 72);
            this.tbSmsSearchTransactionId.Name = "tbSmsSearchTransactionId";
            this.tbSmsSearchTransactionId.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchTransactionId.TabIndex = 24;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(752, 51);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 13);
            this.label32.TabIndex = 23;
            this.label32.Text = "Accept";
            // 
            // cbSmsSearchAccept
            // 
            this.cbSmsSearchAccept.FormattingEnabled = true;
            this.cbSmsSearchAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.cbSmsSearchAccept.Location = new System.Drawing.Point(753, 71);
            this.cbSmsSearchAccept.Name = "cbSmsSearchAccept";
            this.cbSmsSearchAccept.Size = new System.Drawing.Size(153, 21);
            this.cbSmsSearchAccept.TabIndex = 22;
            this.cbSmsSearchAccept.Text = "application/json";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(27, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(15, 13);
            this.label26.TabIndex = 21;
            this.label26.Text = "id";
            // 
            // tbSmsSearchId
            // 
            this.tbSmsSearchId.Location = new System.Drawing.Point(101, 44);
            this.tbSmsSearchId.Name = "tbSmsSearchId";
            this.tbSmsSearchId.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchId.TabIndex = 20;
            // 
            // tbSmsSearchResult
            // 
            this.tbSmsSearchResult.Location = new System.Drawing.Point(15, 271);
            this.tbSmsSearchResult.Multiline = true;
            this.tbSmsSearchResult.Name = "tbSmsSearchResult";
            this.tbSmsSearchResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSmsSearchResult.Size = new System.Drawing.Size(893, 238);
            this.tbSmsSearchResult.TabIndex = 19;
            // 
            // bSmsSearchGET
            // 
            this.bSmsSearchGET.Location = new System.Drawing.Point(755, 13);
            this.bSmsSearchGET.Name = "bSmsSearchGET";
            this.bSmsSearchGET.Size = new System.Drawing.Size(153, 29);
            this.bSmsSearchGET.TabIndex = 18;
            this.bSmsSearchGET.Text = "GET";
            this.bSmsSearchGET.UseVisualStyleBackColor = true;
            this.bSmsSearchGET.Click += new System.EventHandler(this.bSmsSearchGET_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(27, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(20, 13);
            this.label27.TabIndex = 17;
            this.label27.Text = "Url";
            // 
            // tbSmsSearchUrl
            // 
            this.tbSmsSearchUrl.Location = new System.Drawing.Point(101, 18);
            this.tbSmsSearchUrl.Name = "tbSmsSearchUrl";
            this.tbSmsSearchUrl.Size = new System.Drawing.Size(631, 20);
            this.tbSmsSearchUrl.TabIndex = 16;
            this.tbSmsSearchUrl.Text = "https://api.smshosting.it/rest/api/sms/search";
            // 
            // tsbSmsRecvSearch
            // 
            this.tsbSmsRecvSearch.Controls.Add(this.label78);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchLimit);
            this.tsbSmsRecvSearch.Controls.Add(this.label77);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchOffset);
            this.tsbSmsRecvSearch.Controls.Add(this.label76);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchToDate);
            this.tsbSmsRecvSearch.Controls.Add(this.label63);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchAccept);
            this.tsbSmsRecvSearch.Controls.Add(this.label64);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchFromDate);
            this.tsbSmsRecvSearch.Controls.Add(this.label71);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchSimRefId);
            this.tsbSmsRecvSearch.Controls.Add(this.label74);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchFrom);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchResult);
            this.tsbSmsRecvSearch.Controls.Add(this.bSmsRecvSearchGet);
            this.tsbSmsRecvSearch.Controls.Add(this.label75);
            this.tsbSmsRecvSearch.Controls.Add(this.tbSmsRecvSearchUrl);
            this.tsbSmsRecvSearch.Location = new System.Drawing.Point(4, 22);
            this.tsbSmsRecvSearch.Name = "tsbSmsRecvSearch";
            this.tsbSmsRecvSearch.Size = new System.Drawing.Size(922, 522);
            this.tsbSmsRecvSearch.TabIndex = 10;
            this.tsbSmsRecvSearch.Text = "SMS RECV search";
            this.tsbSmsRecvSearch.UseVisualStyleBackColor = true;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(27, 177);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(24, 13);
            this.label78.TabIndex = 37;
            this.label78.Text = "limit";
            // 
            // tbSmsRecvSearchLimit
            // 
            this.tbSmsRecvSearchLimit.Location = new System.Drawing.Point(91, 174);
            this.tbSmsRecvSearchLimit.Name = "tbSmsRecvSearchLimit";
            this.tbSmsRecvSearchLimit.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchLimit.TabIndex = 36;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(27, 151);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(33, 13);
            this.label77.TabIndex = 35;
            this.label77.Text = "offset";
            // 
            // tbSmsRecvSearchOffset
            // 
            this.tbSmsRecvSearchOffset.Location = new System.Drawing.Point(91, 148);
            this.tbSmsRecvSearchOffset.Name = "tbSmsRecvSearchOffset";
            this.tbSmsRecvSearchOffset.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchOffset.TabIndex = 34;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(27, 125);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(39, 13);
            this.label76.TabIndex = 33;
            this.label76.Text = "toDate";
            // 
            // tbSmsRecvSearchToDate
            // 
            this.tbSmsRecvSearchToDate.Location = new System.Drawing.Point(91, 122);
            this.tbSmsRecvSearchToDate.Name = "tbSmsRecvSearchToDate";
            this.tbSmsRecvSearchToDate.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchToDate.TabIndex = 32;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(752, 51);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 31;
            this.label63.Text = "Accept";
            // 
            // tbSmsRecvSearchAccept
            // 
            this.tbSmsRecvSearchAccept.FormattingEnabled = true;
            this.tbSmsRecvSearchAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbSmsRecvSearchAccept.Location = new System.Drawing.Point(753, 71);
            this.tbSmsRecvSearchAccept.Name = "tbSmsRecvSearchAccept";
            this.tbSmsRecvSearchAccept.Size = new System.Drawing.Size(153, 21);
            this.tbSmsRecvSearchAccept.TabIndex = 30;
            this.tbSmsRecvSearchAccept.Text = "application/json";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(27, 99);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(50, 13);
            this.label64.TabIndex = 29;
            this.label64.Text = "fromDate";
            // 
            // tbSmsRecvSearchFromDate
            // 
            this.tbSmsRecvSearchFromDate.Location = new System.Drawing.Point(91, 96);
            this.tbSmsRecvSearchFromDate.Name = "tbSmsRecvSearchFromDate";
            this.tbSmsRecvSearchFromDate.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchFromDate.TabIndex = 28;
            this.tbSmsRecvSearchFromDate.Text = "2015-01-01T00:00:00+0100";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(27, 73);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(48, 13);
            this.label71.TabIndex = 27;
            this.label71.Text = "simIdRef";
            // 
            // tbSmsRecvSearchSimRefId
            // 
            this.tbSmsRecvSearchSimRefId.Location = new System.Drawing.Point(91, 70);
            this.tbSmsRecvSearchSimRefId.Name = "tbSmsRecvSearchSimRefId";
            this.tbSmsRecvSearchSimRefId.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchSimRefId.TabIndex = 26;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(27, 47);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(27, 13);
            this.label74.TabIndex = 25;
            this.label74.Text = "from";
            // 
            // tbSmsRecvSearchFrom
            // 
            this.tbSmsRecvSearchFrom.Location = new System.Drawing.Point(91, 44);
            this.tbSmsRecvSearchFrom.Name = "tbSmsRecvSearchFrom";
            this.tbSmsRecvSearchFrom.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchFrom.TabIndex = 24;
            // 
            // tbSmsRecvSearchResult
            // 
            this.tbSmsRecvSearchResult.Location = new System.Drawing.Point(15, 213);
            this.tbSmsRecvSearchResult.Multiline = true;
            this.tbSmsRecvSearchResult.Name = "tbSmsRecvSearchResult";
            this.tbSmsRecvSearchResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSmsRecvSearchResult.Size = new System.Drawing.Size(893, 296);
            this.tbSmsRecvSearchResult.TabIndex = 23;
            // 
            // bSmsRecvSearchGet
            // 
            this.bSmsRecvSearchGet.Location = new System.Drawing.Point(755, 13);
            this.bSmsRecvSearchGet.Name = "bSmsRecvSearchGet";
            this.bSmsRecvSearchGet.Size = new System.Drawing.Size(153, 29);
            this.bSmsRecvSearchGet.TabIndex = 22;
            this.bSmsRecvSearchGet.Text = "GET";
            this.bSmsRecvSearchGet.UseVisualStyleBackColor = true;
            this.bSmsRecvSearchGet.Click += new System.EventHandler(this.bSmsRecvSearchGet_Click);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(27, 21);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(20, 13);
            this.label75.TabIndex = 21;
            this.label75.Text = "Url";
            // 
            // tbSmsRecvSearchUrl
            // 
            this.tbSmsRecvSearchUrl.Location = new System.Drawing.Point(91, 18);
            this.tbSmsRecvSearchUrl.Name = "tbSmsRecvSearchUrl";
            this.tbSmsRecvSearchUrl.Size = new System.Drawing.Size(641, 20);
            this.tbSmsRecvSearchUrl.TabIndex = 20;
            this.tbSmsRecvSearchUrl.Text = "https://api.smshosting.it/rest/api/sms/received/search";
            // 
            // tabSmsSimList
            // 
            this.tabSmsSimList.Controls.Add(this.label79);
            this.tabSmsSimList.Controls.Add(this.tbSimListAccept);
            this.tabSmsSimList.Controls.Add(this.tbSimListResult);
            this.tabSmsSimList.Controls.Add(this.bSimListGet);
            this.tabSmsSimList.Controls.Add(this.label80);
            this.tabSmsSimList.Controls.Add(this.tbSimListUrl);
            this.tabSmsSimList.Location = new System.Drawing.Point(4, 22);
            this.tabSmsSimList.Name = "tabSmsSimList";
            this.tabSmsSimList.Size = new System.Drawing.Size(922, 522);
            this.tabSmsSimList.TabIndex = 11;
            this.tabSmsSimList.Text = "SMS SIM list";
            this.tabSmsSimList.UseVisualStyleBackColor = true;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(754, 53);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 13);
            this.label79.TabIndex = 25;
            this.label79.Text = "Accept";
            // 
            // tbSimListAccept
            // 
            this.tbSimListAccept.FormattingEnabled = true;
            this.tbSimListAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbSimListAccept.Location = new System.Drawing.Point(755, 73);
            this.tbSimListAccept.Name = "tbSimListAccept";
            this.tbSimListAccept.Size = new System.Drawing.Size(153, 21);
            this.tbSimListAccept.TabIndex = 24;
            this.tbSimListAccept.Text = "application/json";
            // 
            // tbSimListResult
            // 
            this.tbSimListResult.Location = new System.Drawing.Point(15, 143);
            this.tbSimListResult.Multiline = true;
            this.tbSimListResult.Name = "tbSimListResult";
            this.tbSimListResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSimListResult.Size = new System.Drawing.Size(893, 366);
            this.tbSimListResult.TabIndex = 23;
            // 
            // bSimListGet
            // 
            this.bSimListGet.Location = new System.Drawing.Point(755, 13);
            this.bSimListGet.Name = "bSimListGet";
            this.bSimListGet.Size = new System.Drawing.Size(153, 29);
            this.bSimListGet.TabIndex = 22;
            this.bSimListGet.Text = "GET";
            this.bSimListGet.UseVisualStyleBackColor = true;
            this.bSimListGet.Click += new System.EventHandler(this.bSimListGet_Click);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(27, 21);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(20, 13);
            this.label80.TabIndex = 21;
            this.label80.Text = "Url";
            // 
            // tbSimListUrl
            // 
            this.tbSimListUrl.Location = new System.Drawing.Point(67, 18);
            this.tbSimListUrl.Name = "tbSimListUrl";
            this.tbSimListUrl.Size = new System.Drawing.Size(665, 20);
            this.tbSimListUrl.TabIndex = 20;
            this.tbSimListUrl.Text = "https://api.smshosting.it/rest/api/sms/received/sim/list";
            // 
            // tabOtpSend
            // 
            this.tabOtpSend.Controls.Add(this.label31);
            this.tabOtpSend.Controls.Add(this.tbOtpSendAccept);
            this.tabOtpSend.Controls.Add(this.label16);
            this.tabOtpSend.Controls.Add(this.label15);
            this.tabOtpSend.Controls.Add(this.cbOtpSendEncoding);
            this.tabOtpSend.Controls.Add(this.cbOtpSendSandbox);
            this.tabOtpSend.Controls.Add(this.label14);
            this.tabOtpSend.Controls.Add(this.tbOtpSendTtl);
            this.tabOtpSend.Controls.Add(this.label13);
            this.tabOtpSend.Controls.Add(this.tbOtpSendMaxRetry);
            this.tabOtpSend.Controls.Add(this.label12);
            this.tabOtpSend.Controls.Add(this.tbOtpSendCodLen);
            this.tabOtpSend.Controls.Add(this.label11);
            this.tabOtpSend.Controls.Add(this.tbOtpSendAppId);
            this.tabOtpSend.Controls.Add(this.label10);
            this.tabOtpSend.Controls.Add(this.tbOtpSendFrom);
            this.tabOtpSend.Controls.Add(this.label6);
            this.tabOtpSend.Controls.Add(this.tbOtpSendText);
            this.tabOtpSend.Controls.Add(this.label5);
            this.tabOtpSend.Controls.Add(this.tbOtpSendTo);
            this.tabOtpSend.Controls.Add(this.tbSendResult);
            this.tabOtpSend.Controls.Add(this.bOtpSendPost);
            this.tabOtpSend.Controls.Add(this.label4);
            this.tabOtpSend.Controls.Add(this.tbOtpSendUrl);
            this.tabOtpSend.Location = new System.Drawing.Point(4, 22);
            this.tabOtpSend.Name = "tabOtpSend";
            this.tabOtpSend.Padding = new System.Windows.Forms.Padding(3);
            this.tabOtpSend.Size = new System.Drawing.Size(922, 522);
            this.tabOtpSend.TabIndex = 1;
            this.tabOtpSend.Text = "OTP Send";
            this.tabOtpSend.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(752, 51);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 13);
            this.label31.TabIndex = 27;
            this.label31.Text = "Accept";
            // 
            // tbOtpSendAccept
            // 
            this.tbOtpSendAccept.FormattingEnabled = true;
            this.tbOtpSendAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbOtpSendAccept.Location = new System.Drawing.Point(753, 71);
            this.tbOtpSendAccept.Name = "tbOtpSendAccept";
            this.tbOtpSendAccept.Size = new System.Drawing.Size(153, 21);
            this.tbOtpSendAccept.TabIndex = 26;
            this.tbOtpSendAccept.Text = "application/json";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "sandbox";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 174);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "encoding";
            // 
            // cbOtpSendEncoding
            // 
            this.cbOtpSendEncoding.FormattingEnabled = true;
            this.cbOtpSendEncoding.Items.AddRange(new object[] {
            "7BIT",
            "UCS2",
            "AUTO"});
            this.cbOtpSendEncoding.Location = new System.Drawing.Point(89, 171);
            this.cbOtpSendEncoding.Name = "cbOtpSendEncoding";
            this.cbOtpSendEncoding.Size = new System.Drawing.Size(121, 21);
            this.cbOtpSendEncoding.TabIndex = 23;
            this.cbOtpSendEncoding.Text = "7BIT";
            // 
            // cbOtpSendSandbox
            // 
            this.cbOtpSendSandbox.AutoSize = true;
            this.cbOtpSendSandbox.Checked = true;
            this.cbOtpSendSandbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOtpSendSandbox.Location = new System.Drawing.Point(89, 123);
            this.cbOtpSendSandbox.Name = "cbOtpSendSandbox";
            this.cbOtpSendSandbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbOtpSendSandbox.Size = new System.Drawing.Size(15, 14);
            this.cbOtpSendSandbox.TabIndex = 22;
            this.cbOtpSendSandbox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbOtpSendSandbox.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(27, 253);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "ttl";
            // 
            // tbOtpSendTtl
            // 
            this.tbOtpSendTtl.Location = new System.Drawing.Point(89, 250);
            this.tbOtpSendTtl.Name = "tbOtpSendTtl";
            this.tbOtpSendTtl.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendTtl.TabIndex = 20;
            this.tbOtpSendTtl.Text = "300";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 227);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "max_retry";
            // 
            // tbOtpSendMaxRetry
            // 
            this.tbOtpSendMaxRetry.Location = new System.Drawing.Point(89, 224);
            this.tbOtpSendMaxRetry.Name = "tbOtpSendMaxRetry";
            this.tbOtpSendMaxRetry.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendMaxRetry.TabIndex = 18;
            this.tbOtpSendMaxRetry.Text = "3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 201);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "code_len";
            // 
            // tbOtpSendCodLen
            // 
            this.tbOtpSendCodLen.Location = new System.Drawing.Point(89, 198);
            this.tbOtpSendCodLen.Name = "tbOtpSendCodLen";
            this.tbOtpSendCodLen.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendCodLen.TabIndex = 16;
            this.tbOtpSendCodLen.Text = "4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 148);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "app_id";
            // 
            // tbOtpSendAppId
            // 
            this.tbOtpSendAppId.Location = new System.Drawing.Point(89, 145);
            this.tbOtpSendAppId.Name = "tbOtpSendAppId";
            this.tbOtpSendAppId.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendAppId.TabIndex = 14;
            this.tbOtpSendAppId.Text = "test";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "from";
            // 
            // tbOtpSendFrom
            // 
            this.tbOtpSendFrom.Location = new System.Drawing.Point(89, 96);
            this.tbOtpSendFrom.Name = "tbOtpSendFrom";
            this.tbOtpSendFrom.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendFrom.TabIndex = 12;
            this.tbOtpSendFrom.Text = "SMS OTP";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "text";
            // 
            // tbOtpSendText
            // 
            this.tbOtpSendText.Location = new System.Drawing.Point(89, 70);
            this.tbOtpSendText.Name = "tbOtpSendText";
            this.tbOtpSendText.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendText.TabIndex = 10;
            this.tbOtpSendText.Text = "Codice ${verify_code}";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "to";
            // 
            // tbOtpSendTo
            // 
            this.tbOtpSendTo.Location = new System.Drawing.Point(89, 44);
            this.tbOtpSendTo.Name = "tbOtpSendTo";
            this.tbOtpSendTo.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendTo.TabIndex = 8;
            // 
            // tbSendResult
            // 
            this.tbSendResult.Location = new System.Drawing.Point(15, 282);
            this.tbSendResult.Multiline = true;
            this.tbSendResult.Name = "tbSendResult";
            this.tbSendResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSendResult.Size = new System.Drawing.Size(893, 218);
            this.tbSendResult.TabIndex = 7;
            // 
            // bOtpSendPost
            // 
            this.bOtpSendPost.Location = new System.Drawing.Point(755, 13);
            this.bOtpSendPost.Name = "bOtpSendPost";
            this.bOtpSendPost.Size = new System.Drawing.Size(153, 29);
            this.bOtpSendPost.TabIndex = 6;
            this.bOtpSendPost.Text = "POST";
            this.bOtpSendPost.UseVisualStyleBackColor = true;
            this.bOtpSendPost.Click += new System.EventHandler(this.bOtpSendPost_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Url";
            // 
            // tbOtpSendUrl
            // 
            this.tbOtpSendUrl.Location = new System.Drawing.Point(89, 18);
            this.tbOtpSendUrl.Name = "tbOtpSendUrl";
            this.tbOtpSendUrl.Size = new System.Drawing.Size(643, 20);
            this.tbOtpSendUrl.TabIndex = 4;
            this.tbOtpSendUrl.Text = "https://api.smshosting.it/rest/api/verify/send";
            // 
            // tabOtpCheck
            // 
            this.tabOtpCheck.Controls.Add(this.label30);
            this.tabOtpCheck.Controls.Add(this.tbOtpCheckAccept);
            this.tabOtpCheck.Controls.Add(this.label20);
            this.tabOtpCheck.Controls.Add(this.tbOtpCheckIp);
            this.tabOtpCheck.Controls.Add(this.label19);
            this.tabOtpCheck.Controls.Add(this.tbOtpCheckCode);
            this.tabOtpCheck.Controls.Add(this.label18);
            this.tabOtpCheck.Controls.Add(this.tbOtpCheckVerifyId);
            this.tabOtpCheck.Controls.Add(this.tbCheckResult);
            this.tabOtpCheck.Controls.Add(this.bOtpCheckGet);
            this.tabOtpCheck.Controls.Add(this.label17);
            this.tabOtpCheck.Controls.Add(this.tbOtpCheckUrl);
            this.tabOtpCheck.Location = new System.Drawing.Point(4, 22);
            this.tabOtpCheck.Name = "tabOtpCheck";
            this.tabOtpCheck.Size = new System.Drawing.Size(922, 522);
            this.tabOtpCheck.TabIndex = 3;
            this.tabOtpCheck.Text = "OTP Check";
            this.tabOtpCheck.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(752, 51);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 19;
            this.label30.Text = "Accept";
            // 
            // tbOtpCheckAccept
            // 
            this.tbOtpCheckAccept.FormattingEnabled = true;
            this.tbOtpCheckAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbOtpCheckAccept.Location = new System.Drawing.Point(753, 71);
            this.tbOtpCheckAccept.Name = "tbOtpCheckAccept";
            this.tbOtpCheckAccept.Size = new System.Drawing.Size(153, 21);
            this.tbOtpCheckAccept.TabIndex = 18;
            this.tbOtpCheckAccept.Text = "application/json";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 99);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "ip_address";
            // 
            // tbOtpCheckIp
            // 
            this.tbOtpCheckIp.Location = new System.Drawing.Point(91, 96);
            this.tbOtpCheckIp.Name = "tbOtpCheckIp";
            this.tbOtpCheckIp.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCheckIp.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 73);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "verify_code";
            // 
            // tbOtpCheckCode
            // 
            this.tbOtpCheckCode.Location = new System.Drawing.Point(91, 70);
            this.tbOtpCheckCode.Name = "tbOtpCheckCode";
            this.tbOtpCheckCode.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCheckCode.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(27, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "verify_id";
            // 
            // tbOtpCheckVerifyId
            // 
            this.tbOtpCheckVerifyId.Location = new System.Drawing.Point(91, 44);
            this.tbOtpCheckVerifyId.Name = "tbOtpCheckVerifyId";
            this.tbOtpCheckVerifyId.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCheckVerifyId.TabIndex = 8;
            // 
            // tbCheckResult
            // 
            this.tbCheckResult.Location = new System.Drawing.Point(15, 143);
            this.tbCheckResult.Multiline = true;
            this.tbCheckResult.Name = "tbCheckResult";
            this.tbCheckResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbCheckResult.Size = new System.Drawing.Size(893, 366);
            this.tbCheckResult.TabIndex = 7;
            // 
            // bOtpCheckGet
            // 
            this.bOtpCheckGet.Location = new System.Drawing.Point(755, 13);
            this.bOtpCheckGet.Name = "bOtpCheckGet";
            this.bOtpCheckGet.Size = new System.Drawing.Size(153, 29);
            this.bOtpCheckGet.TabIndex = 6;
            this.bOtpCheckGet.Text = "GET";
            this.bOtpCheckGet.UseVisualStyleBackColor = true;
            this.bOtpCheckGet.Click += new System.EventHandler(this.bOtpCheckGet_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(27, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Url";
            // 
            // tbOtpCheckUrl
            // 
            this.tbOtpCheckUrl.Location = new System.Drawing.Point(91, 18);
            this.tbOtpCheckUrl.Name = "tbOtpCheckUrl";
            this.tbOtpCheckUrl.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCheckUrl.TabIndex = 4;
            this.tbOtpCheckUrl.Text = "https://api.smshosting.it/rest/api/verify/check";
            // 
            // tabOtpSearch
            // 
            this.tabOtpSearch.Controls.Add(this.label28);
            this.tabOtpSearch.Controls.Add(this.cbOtpSearchAccept);
            this.tabOtpSearch.Controls.Add(this.label21);
            this.tabOtpSearch.Controls.Add(this.tbOtpSearchVerifyId);
            this.tabOtpSearch.Controls.Add(this.tbSearchResult);
            this.tabOtpSearch.Controls.Add(this.bOtpSearchGet);
            this.tabOtpSearch.Controls.Add(this.label22);
            this.tabOtpSearch.Controls.Add(this.tbOtpSearchUrl);
            this.tabOtpSearch.Location = new System.Drawing.Point(4, 22);
            this.tabOtpSearch.Name = "tabOtpSearch";
            this.tabOtpSearch.Size = new System.Drawing.Size(922, 522);
            this.tabOtpSearch.TabIndex = 4;
            this.tabOtpSearch.Text = "OTP Search";
            this.tabOtpSearch.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(754, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 13);
            this.label28.TabIndex = 17;
            this.label28.Text = "Accept";
            // 
            // cbOtpSearchAccept
            // 
            this.cbOtpSearchAccept.FormattingEnabled = true;
            this.cbOtpSearchAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.cbOtpSearchAccept.Location = new System.Drawing.Point(755, 70);
            this.cbOtpSearchAccept.Name = "cbOtpSearchAccept";
            this.cbOtpSearchAccept.Size = new System.Drawing.Size(153, 21);
            this.cbOtpSearchAccept.TabIndex = 16;
            this.cbOtpSearchAccept.Text = "application/json";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(27, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = "verify_id";
            // 
            // tbOtpSearchVerifyId
            // 
            this.tbOtpSearchVerifyId.Location = new System.Drawing.Point(91, 44);
            this.tbOtpSearchVerifyId.Name = "tbOtpSearchVerifyId";
            this.tbOtpSearchVerifyId.Size = new System.Drawing.Size(641, 20);
            this.tbOtpSearchVerifyId.TabIndex = 14;
            // 
            // tbSearchResult
            // 
            this.tbSearchResult.Location = new System.Drawing.Point(15, 143);
            this.tbSearchResult.Multiline = true;
            this.tbSearchResult.Name = "tbSearchResult";
            this.tbSearchResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbSearchResult.Size = new System.Drawing.Size(893, 366);
            this.tbSearchResult.TabIndex = 13;
            // 
            // bOtpSearchGet
            // 
            this.bOtpSearchGet.Location = new System.Drawing.Point(755, 13);
            this.bOtpSearchGet.Name = "bOtpSearchGet";
            this.bOtpSearchGet.Size = new System.Drawing.Size(153, 29);
            this.bOtpSearchGet.TabIndex = 12;
            this.bOtpSearchGet.Text = "GET";
            this.bOtpSearchGet.UseVisualStyleBackColor = true;
            this.bOtpSearchGet.Click += new System.EventHandler(this.bOtpSearchGet_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(27, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 13);
            this.label22.TabIndex = 11;
            this.label22.Text = "Url";
            // 
            // tbOtpSearchUrl
            // 
            this.tbOtpSearchUrl.Location = new System.Drawing.Point(91, 18);
            this.tbOtpSearchUrl.Name = "tbOtpSearchUrl";
            this.tbOtpSearchUrl.Size = new System.Drawing.Size(641, 20);
            this.tbOtpSearchUrl.TabIndex = 10;
            this.tbOtpSearchUrl.Text = "https://api.smshosting.it/rest/api/verify/search";
            // 
            // tabOtpCommand
            // 
            this.tabOtpCommand.Controls.Add(this.label29);
            this.tabOtpCommand.Controls.Add(this.tbOtpCommandAccept);
            this.tabOtpCommand.Controls.Add(this.label25);
            this.tabOtpCommand.Controls.Add(this.tbOtpCommandCommand);
            this.tabOtpCommand.Controls.Add(this.label23);
            this.tabOtpCommand.Controls.Add(this.tbOtpCommandVerifyId);
            this.tabOtpCommand.Controls.Add(this.tbCommandResult);
            this.tabOtpCommand.Controls.Add(this.bOtpCommandPost);
            this.tabOtpCommand.Controls.Add(this.label24);
            this.tabOtpCommand.Controls.Add(this.tbOtpCommandUrl);
            this.tabOtpCommand.Location = new System.Drawing.Point(4, 22);
            this.tabOtpCommand.Name = "tabOtpCommand";
            this.tabOtpCommand.Size = new System.Drawing.Size(922, 522);
            this.tabOtpCommand.TabIndex = 5;
            this.tabOtpCommand.Text = "OTP Command";
            this.tabOtpCommand.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(752, 51);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 13);
            this.label29.TabIndex = 25;
            this.label29.Text = "Accept";
            // 
            // tbOtpCommandAccept
            // 
            this.tbOtpCommandAccept.FormattingEnabled = true;
            this.tbOtpCommandAccept.Items.AddRange(new object[] {
            "application/json",
            "application/xml"});
            this.tbOtpCommandAccept.Location = new System.Drawing.Point(753, 71);
            this.tbOtpCommandAccept.Name = "tbOtpCommandAccept";
            this.tbOtpCommandAccept.Size = new System.Drawing.Size(153, 21);
            this.tbOtpCommandAccept.TabIndex = 24;
            this.tbOtpCommandAccept.Text = "application/json";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(27, 73);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "command";
            // 
            // tbOtpCommandCommand
            // 
            this.tbOtpCommandCommand.Location = new System.Drawing.Point(91, 70);
            this.tbOtpCommandCommand.Name = "tbOtpCommandCommand";
            this.tbOtpCommandCommand.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCommandCommand.TabIndex = 22;
            this.tbOtpCommandCommand.Text = "DELETE";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(27, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "verify_id";
            // 
            // tbOtpCommandVerifyId
            // 
            this.tbOtpCommandVerifyId.Location = new System.Drawing.Point(91, 44);
            this.tbOtpCommandVerifyId.Name = "tbOtpCommandVerifyId";
            this.tbOtpCommandVerifyId.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCommandVerifyId.TabIndex = 20;
            // 
            // tbCommandResult
            // 
            this.tbCommandResult.Location = new System.Drawing.Point(15, 98);
            this.tbCommandResult.Multiline = true;
            this.tbCommandResult.Name = "tbCommandResult";
            this.tbCommandResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbCommandResult.Size = new System.Drawing.Size(893, 411);
            this.tbCommandResult.TabIndex = 19;
            // 
            // bOtpCommandPost
            // 
            this.bOtpCommandPost.Location = new System.Drawing.Point(755, 13);
            this.bOtpCommandPost.Name = "bOtpCommandPost";
            this.bOtpCommandPost.Size = new System.Drawing.Size(153, 29);
            this.bOtpCommandPost.TabIndex = 18;
            this.bOtpCommandPost.Text = "POST";
            this.bOtpCommandPost.UseVisualStyleBackColor = true;
            this.bOtpCommandPost.Click += new System.EventHandler(this.bOtpCommandPost_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(27, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(20, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Url";
            // 
            // tbOtpCommandUrl
            // 
            this.tbOtpCommandUrl.Location = new System.Drawing.Point(91, 18);
            this.tbOtpCommandUrl.Name = "tbOtpCommandUrl";
            this.tbOtpCommandUrl.Size = new System.Drawing.Size(641, 20);
            this.tbOtpCommandUrl.TabIndex = 16;
            this.tbOtpCommandUrl.Text = "https://api.smshosting.it/rest/api/verify/command";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(881, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 28);
            this.button1.TabIndex = 5;
            this.button1.Text = "save setting";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(881, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 28);
            this.button2.TabIndex = 6;
            this.button2.Text = "load setting";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 625);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbSmsSearchAccept);
            this.Controls.Add(this.tbAuth_secret);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbAuth_key);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "iContact sample";
            this.tbSmsSearchAccept.ResumeLayout(false);
            this.tabUser.ResumeLayout(false);
            this.tabUser.PerformLayout();
            this.tabSmsSend.ResumeLayout(false);
            this.tabSmsSend.PerformLayout();
            this.tabSmsSendbulk.ResumeLayout(false);
            this.tabSmsSendbulk.PerformLayout();
            this.tabSmsEstimate.ResumeLayout(false);
            this.tabSmsEstimate.PerformLayout();
            this.tabSmsCancel.ResumeLayout(false);
            this.tabSmsCancel.PerformLayout();
            this.tabSmsSearch.ResumeLayout(false);
            this.tabSmsSearch.PerformLayout();
            this.tsbSmsRecvSearch.ResumeLayout(false);
            this.tsbSmsRecvSearch.PerformLayout();
            this.tabSmsSimList.ResumeLayout(false);
            this.tabSmsSimList.PerformLayout();
            this.tabOtpSend.ResumeLayout(false);
            this.tabOtpSend.PerformLayout();
            this.tabOtpCheck.ResumeLayout(false);
            this.tabOtpCheck.PerformLayout();
            this.tabOtpSearch.ResumeLayout(false);
            this.tabOtpSearch.PerformLayout();
            this.tabOtpCommand.ResumeLayout(false);
            this.tabOtpCommand.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAuth_key;
        private System.Windows.Forms.TextBox tbAuth_secret;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tbSmsSearchAccept;
        private System.Windows.Forms.TabPage tabUser;
        private System.Windows.Forms.TextBox tbUserResult;
        private System.Windows.Forms.Button bGetUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbUserUrl;
        private System.Windows.Forms.TabPage tabOtpSend;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOtpSendText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbOtpSendTo;
        private System.Windows.Forms.TextBox tbSendResult;
        private System.Windows.Forms.Button bOtpSendPost;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbOtpSendUrl;
        private System.Windows.Forms.TabPage tabSmsSend;
        private System.Windows.Forms.TextBox tbSmsSendResult;
        private System.Windows.Forms.ComboBox cbOtpSendEncoding;
        private System.Windows.Forms.CheckBox cbOtpSendSandbox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbOtpSendTtl;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbOtpSendMaxRetry;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbOtpSendCodLen;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbOtpSendAppId;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbOtpSendFrom;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabOtpCheck;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbOtpCheckIp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbOtpCheckCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbOtpCheckVerifyId;
        private System.Windows.Forms.TextBox tbCheckResult;
        private System.Windows.Forms.Button bOtpCheckGet;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbOtpCheckUrl;
        private System.Windows.Forms.TabPage tabOtpSearch;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbOtpSearchVerifyId;
        private System.Windows.Forms.TextBox tbSearchResult;
        private System.Windows.Forms.Button bOtpSearchGet;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbOtpSearchUrl;
        private System.Windows.Forms.TabPage tabOtpCommand;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbOtpCommandCommand;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbOtpCommandVerifyId;
        private System.Windows.Forms.TextBox tbCommandResult;
        private System.Windows.Forms.Button bOtpCommandPost;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbOtpCommandUrl;
        private System.Windows.Forms.TabPage tabSmsSearch;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbSmsSearchId;
        private System.Windows.Forms.TextBox tbSmsSearchResult;
        private System.Windows.Forms.Button bSmsSearchGET;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbSmsSearchUrl;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbOtpSearchAccept;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox tbUserAccept;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cbSmsSearchAccept;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox tbOtpSendAccept;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox tbOtpCheckAccept;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox tbOtpCommandAccept;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox tbSmsSendAccept;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox tbSmsSendEncoding;
        private System.Windows.Forms.CheckBox tbSmsSendSandbox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbSmsSendCallback;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbSmsSendTransaction;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbSmsSendDate;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbSmsSendGroup;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbSmsSendFrom;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbSmsSendText;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbSmsSendTo;
        private System.Windows.Forms.Button bSmsSendPost;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbSmsSendUrl;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox tbSmsSearchLimit;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbSmsSearchOffset;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox tbSmsSearchStatus;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox tbSmsSearchToDate;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbSmsSearchFromDate;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbSmsSearchMsisdn;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbSmsSearchTransactionId;
        private System.Windows.Forms.TabPage tabSmsSendbulk;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.ComboBox tbSmsSendbulkAccept;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox tbSmsSendbulkEncoding;
        private System.Windows.Forms.CheckBox tbSmsSendbulkSandbox;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox tbSmsSendbulkCallback;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox tbSmsSendbulkTransaction;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox tbSmsSendbulkDate;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox tbSmsSendbulkGroup;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox tbSmsSendbulkFrom;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox tbSmsSendbulkText;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox tbSmsSendbulkTo;
        private System.Windows.Forms.Button bSmsSendbulkPost;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tbSmsSendbulkUrl;
        private System.Windows.Forms.TextBox tbSmsSendbulkResult;
        private System.Windows.Forms.TabPage tabSmsEstimate;
        private System.Windows.Forms.TabPage tabSmsCancel;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox tbSmsEstimateAccept;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ComboBox tbSmsEstimateEncoding;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox tbSmsEstimateGroup;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox tbSmsEstimateFrom;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox tbSmsEstimateText;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox tbSmsEstimateTo;
        private System.Windows.Forms.Button bSmsEstimatePost;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox tbSmsEstimateUrl;
        private System.Windows.Forms.TextBox tbSmsEstimateResult;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ComboBox tbSmsCancelAccept;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox tbSmsCancelId;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox tbSmsCancelTransaction;
        private System.Windows.Forms.Button bSmsCancelPost;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox tbSmsCancelUrl;
        private System.Windows.Forms.TextBox tbSmsCancelResult;
        private System.Windows.Forms.TabPage tsbSmsRecvSearch;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox tbSmsRecvSearchLimit;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox tbSmsRecvSearchOffset;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox tbSmsRecvSearchToDate;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox tbSmsRecvSearchAccept;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox tbSmsRecvSearchFromDate;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox tbSmsRecvSearchSimRefId;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox tbSmsRecvSearchFrom;
        private System.Windows.Forms.TextBox tbSmsRecvSearchResult;
        private System.Windows.Forms.Button bSmsRecvSearchGet;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox tbSmsRecvSearchUrl;
        private System.Windows.Forms.TabPage tabSmsSimList;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox tbSimListAccept;
        private System.Windows.Forms.TextBox tbSimListResult;
        private System.Windows.Forms.Button bSimListGet;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox tbSimListUrl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}